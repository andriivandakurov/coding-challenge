export interface IInputSearch {
  value: string;
  onTextChange: (text: string) => void;
}
