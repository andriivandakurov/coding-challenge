import { FC } from 'react';
import { Input, SearchWrapper } from './styled';
import { ReactComponent as IconSearch } from '../../assets/search.svg';
import { IInputSearch } from './types';

export const InputSearch: FC<IInputSearch> = ({
  value,
  onTextChange,
}): JSX.Element => {
  return (
    <SearchWrapper>
      <IconSearch />
      <Input
        value={value}
        placeholder="Search for dishes..."
        onChange={(e) => onTextChange(e.target.value)}
      />
    </SearchWrapper>
  );
};
