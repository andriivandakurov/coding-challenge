import styled from 'styled-components';

export const Input = styled.input`
  font-family: ${(props) => props.theme.typography.fontFamily};
  text-color: ${(props) => props.theme.colors.textPrimary};
  outline: none;
  border: none;
  line-height: 21px;
  margin-left: 8px;
  font-size: 14px;
  ::placeholder: ${(props) => props.theme.colors.textSecondary};
`;

export const SearchWrapper = styled.div`
  border: 2px solid ${(props) => props.theme.colors.greyLight};
  border-radius: 7px;
  box-sizing: border-box;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  height: 49px;
  background: white;
  margin-left: ${(props) => props.theme.spacing.contentHorizontal};
  margin-right: ${(props) => props.theme.spacing.contentHorizontal};
  padding: 0 14px;
`;
