import React from 'react';
import { RotatedBlock, LoaderWrapper } from './styled';
import { ReactComponent as LoaderIcon } from '../../assets/spinner.svg';
import { ILoader } from './types';

export const Loader = ({ isLoading, children }: ILoader): JSX.Element => {
  if (!isLoading) return <>{children}</>;

  return (
    <LoaderWrapper>
      <RotatedBlock>
        <LoaderIcon width={30} height={30} />
      </RotatedBlock>
    </LoaderWrapper>
  );
};
