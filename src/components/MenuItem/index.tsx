import React from 'react';
import {
  Cart,
  CartSelected,
  CartTitle,
  Price,
  PriceOld,
  CartInfo,
  CartImg,
  CartDescription,
} from './styled';
import { IMenuItem } from './types';

export const MenuItem = ({
  id,
  category_id,
  name,
  price,
  discount_rate,
  description,
  photo,
  onSelect,
  isSelected,
  amount,
}: IMenuItem) => {
  const currentPrice = discount_rate ? price - price * discount_rate : price;
  const CartComponent = isSelected ? CartSelected : Cart;

  return (
    <CartComponent onClick={() => onSelect({ id, category_id })}>
      <CartInfo>
        <CartTitle>
          {amount ? `${amount} x ` : ''}
          {name}
        </CartTitle>
        <CartDescription>{description}</CartDescription>
        <div>
          <Price>AED {currentPrice}</Price>
          {Boolean(discount_rate) && <PriceOld>AED {price}</PriceOld>}
        </div>
      </CartInfo>
      {Boolean(photo) && <CartImg src={photo as string} alt={name} />}
    </CartComponent>
  );
};
