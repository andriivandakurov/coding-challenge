import styled from 'styled-components';
import { ICartProps, IPriceProps } from './types';

export const Cart = styled.div<ICartProps>`
  display: flex;
  align-items: center;
  border: 0 solid transparent;
  padding-left: ${(props) => props.theme.spacing.contentHorizontal};
  padding-right: ${(props) => props.theme.spacing.contentHorizontal};
  cursor: pointer;
`;

export const CartSelected = styled(Cart)`
  border-left-width: 7px;
  border-left-color: ${(props) => props.theme.colors.selectedMenuItem};
  padding-left: calc(${(props) => props.theme.spacing.contentHorizontal} - 7px);
`;

export const CartInfo = styled.div`
  flex-grow: 1;
`;

export const CartTitle = styled.h3`
  font-size: 16px;
  font-weight: 600;
  line-height: 21px;
  font-style: normal;
  margin: 0 0 7px 0;
`;
export const CartDescription = styled.p`
  margin: 7px 0 7px 0;
`;

export const Price = styled.span<IPriceProps>`
  font-weight: bold;
  margin-right: 14px;
`;

export const PriceOld = styled(Price)`
  text-decoration: line-through;
  font-weight: normal;
`;

export const CartImg = styled.img`
  border-radius: 7px;
  height: 91px;
  width: 91px;
  margin-left: 7px;
`;
