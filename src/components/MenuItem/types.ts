import { ISelectedMenuItem } from '../../reducers/types';

export interface ICartProps {
  isSelected?: boolean;
}

export interface IPriceProps {
  isOld?: boolean;
}

export interface IMenuItemProps {
  id: string;
  name: string;
  url: string;
  price: number;
  discount_rate: number;
  stock: {
    availability: number;
  };
  description: string;
  photo: string | null;
  category_id: string;
}

export interface IMenuItem extends IMenuItemProps {
  onSelect: (item: ISelectedMenuItem) => void;
  amount?: number;
  isSelected: boolean;
}
