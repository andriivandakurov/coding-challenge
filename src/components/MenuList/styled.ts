import styled from 'styled-components';

export const MenuList = styled.ul`
  list-style-type: none;
  padding: 0;
  margin: 0;

  li {
    padding: 21px 0;
    position: relative;

    &:after {
      content: '';
      position: absolute;
      left: ${(props) => props.theme.spacing.contentHorizontal};
      right: ${(props) => props.theme.spacing.contentHorizontal};
      border-bottom: 1px solid
        ${(props) => props.theme.colors.cartSeparatorLine};
      bottom: 0;
    }
  }
`;

export const MenuGroup = styled.section`
  & li:last-of-type:after {
    display: none;
  }

  & li:first-of-type {
    padding-top: 0;
  }
`;

export const EmptyState = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  min-height: 200px;
`;
