import React from 'react';
import { MenuItem, Title } from '../../components';
import { IMenuList } from './types';
import { MenuList as MenuListStyled, MenuGroup, EmptyState } from './styled';
import { getCategoryItems } from '../../utils';

export const MenuList = ({ data, selectedItems, onSelect }: IMenuList) => {
  if (!data.length) return <EmptyState>Oops, nothing found ...</EmptyState>;

  return (
    <>
      {data.map((category) => {
        return (
          <MenuGroup key={category.id}>
            <Title>{category.name}</Title>
            <MenuListStyled>
              {getCategoryItems(category.items, category.id).map((item) => {
                const previouslySelectedItem =
                  selectedItems[`${category.id}-${item.id}`];

                return (
                  <li key={item.id}>
                    <MenuItem
                      {...item}
                      onSelect={onSelect}
                      isSelected={Boolean(previouslySelectedItem)}
                      amount={
                        previouslySelectedItem
                          ? previouslySelectedItem.amount
                          : 0
                      }
                    />
                  </li>
                );
              })}
            </MenuListStyled>
          </MenuGroup>
        );
      })}
    </>
  );
};
