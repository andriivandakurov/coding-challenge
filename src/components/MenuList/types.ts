import { ICategoryData, ISelectedMenuItem } from '../../reducers/types';

export interface IMenuList {
  data: ICategoryData[];
  selectedItems: any;
  onSelect: (item: ISelectedMenuItem) => void;
}
