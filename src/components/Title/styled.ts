import styled from 'styled-components';

export const Title = styled.h2`
  color: ${(props) => props.theme.colors.textPrimary};
  margin: 21px;
  margin-left: ${(props) => props.theme.spacing.contentHorizontal};
  margin-right: ${(props) => props.theme.spacing.contentHorizontal};
`;

export const TitleSearch = styled(Title)`
  margin-bottom: 28px;
`;
