import styled from 'styled-components';

export const PageWraper = styled.div`
  max-width: 375px;
`;

export const BtnBack = styled.a`
  margin: 56px ${(props) => props.theme.spacing.contentHorizontal} 7px
    ${(props) => props.theme.spacing.contentHorizontal};
  display: inline-block;
`;
