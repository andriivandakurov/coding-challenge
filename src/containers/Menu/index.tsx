import { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { InputSearch, MenuList, TitleSearch, Loader } from '../../components';
import { IAppState } from '../../store';
import {
  fetchMenuData,
  searchMenuItems,
  selectMenuItem,
  restoreSelectedItems,
  resetState,
} from '../../actions';
import { PageWraper, BtnBack } from './styled';
import { filterMenuData, storage } from '../../utils';
import { ISelectedMenuItem } from '../../reducers/types';
import { ReactComponent as IconBtnBack } from '../../assets/back.svg';

export const Menu = () => {
  const menuCategories = useSelector(
    (state: IAppState) => state.menu.categories
  );
  const searchText = useSelector((state: IAppState) => state.menu.searchText);
  const selectedItems = useSelector(
    (state: IAppState) => state.menu.selectedItems
  );

  const dispatch = useDispatch();
  const handleSearchTextChange = (text: string) =>
    dispatch(searchMenuItems(text));
  const handleMenuItemSelect = (item: ISelectedMenuItem) =>
    dispatch(selectMenuItem(item));
  const handleBtnBackClick = () => dispatch(resetState());

  useEffect(() => {
    const _selectedItems = storage.getItem('selectedItems');

    if (_selectedItems) {
      dispatch(restoreSelectedItems(_selectedItems));
    }

    dispatch(fetchMenuData());
  }, [dispatch]);

  return (
    <PageWraper>
      <BtnBack onClick={handleBtnBackClick}>
        <IconBtnBack />
      </BtnBack>
      <TitleSearch>Search</TitleSearch>
      <InputSearch value={searchText} onTextChange={handleSearchTextChange} />
      <Loader isLoading={Boolean(!menuCategories?.length)}>
        <MenuList
          data={filterMenuData(menuCategories, searchText)}
          selectedItems={selectedItems}
          onSelect={handleMenuItemSelect}
        />
      </Loader>
    </PageWraper>
  );
};
