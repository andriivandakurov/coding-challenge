import React from 'react';
import { GlobalStyles } from './globalStyles';
import { Menu } from './containers/Menu';

function App() {
  return (
    <>
      <GlobalStyles />
      <Menu />
    </>
  );
}

export default App;
