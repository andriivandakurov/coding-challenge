import { menuReducer } from './';
import {
  RECEIVE_MENU_DATA,
  SEARCH_MENU_ITEMS,
  SELECT_MENU_ITEM,
} from '../../actions/menuActions';
import { mockDataCategoriesFormatted } from '../../mocks';

const initialState = {
  selectedItems: {},
  searchText: '',
  categories: [],
};

describe('Menu reducer', () => {
  beforeEach(() => {
    menuReducer(undefined, { type: '' });
  });

  it('should return initial state', () => {
    expect(menuReducer(undefined, { type: '' })).toEqual(initialState);
  });

  it('should handle RECEIVE_MENU_DATA', () => {
    const action = {
      type: RECEIVE_MENU_DATA,
      payload: mockDataCategoriesFormatted,
    };
    const expected = {
      selectedItems: {},
      searchText: '',
      categories: mockDataCategoriesFormatted,
    };

    expect(menuReducer(undefined, action)).toEqual(expected);
  });

  it('should handle SEARCH_MENU_ITEMS', () => {
    const action = {
      type: SEARCH_MENU_ITEMS,
      payload: 'Avo',
    };
    const expected = {
      selectedItems: {},
      searchText: 'Avo',
      categories: [],
    };

    expect(menuReducer(undefined, action)).toEqual(expected);
  });

  it('should handle SELECT_MENU_ITEM', () => {
    const action = {
      type: SELECT_MENU_ITEM,
      payload: {
        id: '1',
        category_id: '1',
      },
    };
    const setAmount = (amount: number) => ({
      selectedItems: {
        '1-1': {
          id: '1',
          category_id: '1',
          amount,
        },
      },
      searchText: '',
      categories: [],
    });

    const state = menuReducer(undefined, action);

    expect(state).toEqual(setAmount(1));
    expect(menuReducer(state, action)).toEqual(setAmount(2));
  });

  it('should be possible to select only available amount', () => {
    const action = {
      type: SELECT_MENU_ITEM,
      payload: {
        id: '1',
        category_id: '1',
      },
    };
    const receiveMenuDataAction = {
      type: RECEIVE_MENU_DATA,
      payload: mockDataCategoriesFormatted,
    };
    const setAmount = (amount: number) => ({
      selectedItems: {
        '1-1': {
          id: '1',
          category_id: '1',
          amount,
        },
      },
      searchText: '',
      categories: mockDataCategoriesFormatted,
    });

    const state = menuReducer(undefined, receiveMenuDataAction);
    const state1 = menuReducer(state, action);
    expect(state1).toEqual(setAmount(1));

    const state2 = menuReducer(state1, action);
    expect(state2).toEqual(setAmount(2));

    const state3 = menuReducer(state2, action);
    expect(state3).toEqual(setAmount(3));

    // 3 - is a available mount in mocked data
    const state4 = menuReducer(state3, action);
    expect(state4).toEqual(setAmount(3));
  });
});
