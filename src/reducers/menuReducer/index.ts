import { IMenuState } from './types';
import { IAction } from '../../actions/types';
import { ISelectedMenuItem, ICategoryData } from '../../reducers/types';
import {
  RECEIVE_MENU_DATA,
  SEARCH_MENU_ITEMS,
  SELECT_MENU_ITEM,
  RESTORE_SELECTED_ITEMS,
  RESET_STATE,
} from '../../actions';
import { storage } from '../../utils';

const initialState: IMenuState = {
  selectedItems: {},
  searchText: '',
  categories: [],
};

export const menuReducer = (
  state = initialState,
  action: IAction<any>
): IMenuState => {
  switch (action.type) {
    case RECEIVE_MENU_DATA:
      return {
        ...state,
        categories: action.payload,
      };
    case SEARCH_MENU_ITEMS:
      return {
        ...state,
        searchText: action.payload,
      };
    case SELECT_MENU_ITEM:
      return handleSelectMenuItem(state, action.payload);
    case RESTORE_SELECTED_ITEMS:
      return {
        ...state,
        selectedItems: action.payload,
      };
    case RESET_STATE:
      storage.clear();
      return {
        ...initialState,
        categories: state.categories,
      };
    default:
      return state;
  }
};

const handleSelectMenuItem = (
  state: IMenuState,
  selectedItem: ISelectedMenuItem
) => {
  const { id, category_id } = selectedItem;
  const _selectedItems = { ...state.selectedItems };
  const currentItem = _selectedItems[`${category_id}-${id}`];
  const currentCategory:
    | ICategoryData
    | undefined = (state.categories as ICategoryData[]).find(
    (category) => category.id === category_id
  );
  const item = currentCategory?.items.find((item) => item.id === id);

  if (item && currentItem?.amount === item?.stock.availability) return state;

  _selectedItems[`${category_id}-${id}`] = {
    ...selectedItem,
    amount: currentItem ? ++currentItem.amount! : 1,
  };

  storage.setItem('selectedItems', _selectedItems);

  return {
    ...state,
    selectedItems: { ..._selectedItems },
  };
};
