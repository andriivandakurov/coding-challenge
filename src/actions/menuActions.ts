import { ThunkAction } from 'redux-thunk';
import {
  IMenuData,
  IMenuState,
  ICategoryData,
  ISelectedMenuItem,
  IRestoreSelectedItems,
} from '../reducers/types';
import { request, formatMenuData } from '../utils';
import { IAction } from './types';

export const MENU_FILTER_ITEMS = 'MENU_FILTER_ITEMS';
export const menuFilterItems = (payload: string): IAction<string> => ({
  type: MENU_FILTER_ITEMS,
  payload,
});

export const SHOW_LOADER = 'SHOW_LOADER';
export const showLoader = (): IAction<null> => ({
  type: SHOW_LOADER,
});

export const HIDE_LOADER = 'HIDE_LOADER';
export const hideLoader = (): IAction<null> => ({
  type: HIDE_LOADER,
});

export const RECEIVE_MENU_DATA = 'RECEIVE_MENU_DATA';
export const receiveMenuData = (
  payload: ICategoryData[]
): IAction<ICategoryData[]> => ({
  type: RECEIVE_MENU_DATA,
  payload,
});

export const FETCH_MENU_DATA = 'FETCH_MENU_DATA';
export const fetchMenuData = (): ThunkAction<
  void,
  IMenuState,
  unknown,
  IAction<ICategoryData[] | null>
> => async (dispatch) => {
  dispatch(showLoader());

  try {
    const resp: IMenuData = await request.get(
      'https://private-209cd-chatfood.apiary-mock.com/menu'
    );
    const formattedData = formatMenuData(resp);

    dispatch(hideLoader());
    dispatch(receiveMenuData(formattedData));
  } catch (err) {
    dispatch(hideLoader());
  }
};

export const SEARCH_MENU_ITEMS = 'SEARCH_MENU_ITEMS';
export const searchMenuItems = (payload: string): IAction<string> => ({
  type: SEARCH_MENU_ITEMS,
  payload,
});

export const SELECT_MENU_ITEM = 'SELECT_MENU_ITEM';
export const selectMenuItem = (
  payload: ISelectedMenuItem
): IAction<ISelectedMenuItem> => ({
  type: SELECT_MENU_ITEM,
  payload,
});

export const RESTORE_SELECTED_ITEMS = 'RESTORE_SELECTED_ITEMS';
export const restoreSelectedItems = (
  payload: IRestoreSelectedItems
): IAction<IRestoreSelectedItems> => ({
  type: RESTORE_SELECTED_ITEMS,
  payload,
});

export const RESET_STATE = 'RESET_STATE';
export const resetState = (): IAction<null> => ({
  type: RESET_STATE,
});
