class Storage {
  public static getItem(key: string) {
    try {
      const res = localStorage.getItem(key);
      return JSON.parse(res || 'null') as any;
    } catch (e) {
      console.error('Failed to get item from localStorage:', e);
      return null;
    }
  }

  public static setItem(key: string, value: any) {
    try {
      return localStorage.setItem(key, JSON.stringify(value));
    } catch (e) {
      console.error('Failed to set item to localStorage:', e);
      return null;
    }
  }

  public static clear(omittedNames?: String[]) {
    try {
      if (!omittedNames) {
        return localStorage.clear();
      }

      const allKeys = Object.keys(localStorage);
      const keysToDelete = allKeys.filter((key) => !omittedNames.includes(key));

      localStorage.multiRemove(keysToDelete);
    } catch (e) {
      console.error('Failed to clear localStorage:', e);
      return null;
    }
  }
}

export const storage = Storage;
