import { IMenuItems, IMenuData } from '../reducers/types';
import orderBy from 'lodash/orderBy';
import { ICategoryData } from '../reducers/types';

export const getCategoryItems = (items: IMenuItems[], categoryId: string) => {
  return items.filter((item) => item.category_id === categoryId);
};

export const formatMenuData = ({
  items,
  categories,
}: IMenuData): ICategoryData[] => {
  const sortedItems = orderBy(items, 'name', 'asc');

  return orderBy(categories, 'name', 'asc').map((category) => {
    return {
      ...category,
      items: sortedItems.filter((item) => item.category_id === category.id),
    };
  });
};

export const filterMenuData = (data: ICategoryData[], searchText: string) => {
  if (!searchText) return data;

  const result = data.reduce(
    (acc: ICategoryData[], category): ICategoryData[] => {
      const filteredItems = category.items.filter((item) =>
        item.name.toLocaleLowerCase().includes(searchText.toLocaleLowerCase())
      );

      if (filteredItems.length) {
        acc.push({ ...category, items: filteredItems });
      }

      return acc;
    },
    []
  );

  return result;
};
