import {
  getCategoryItems,
  formatMenuData,
  filterMenuData,
} from './dataFormatters';
import {
  mockDataMenuItems,
  mockDataCategories,
  mockDataCategoriesFormatted,
} from '../mocks';

describe('Data formatters', () => {
  it('should get category items', () => {
    const testRes = getCategoryItems(mockDataMenuItems, '1');
    const testRes2 = getCategoryItems(mockDataMenuItems, '4');
    const testRes3 = getCategoryItems(mockDataMenuItems, '2');

    expect(testRes.length).toBe(2);
    expect(testRes2.length).toBe(0);
    expect(testRes3.length).toBe(1);
  });

  it('should format menu data', () => {
    const currentResult = formatMenuData({
      items: mockDataMenuItems,
      categories: mockDataCategories,
    });

    expect(currentResult).toEqual(mockDataCategoriesFormatted);
  });

  it('should filter menu by existing text', () => {
    const formattedMenuData = formatMenuData({
      items: mockDataMenuItems,
      categories: mockDataCategories,
    });
    const currentResult = filterMenuData(formattedMenuData, 'Avo');
    const expectedResult = [
      {
        id: '1',
        name: 'Burgers',
        url: 'burgers',
        items: [
          {
            category_id: '1',
            description:
              'Grilled chicken, avocado, tomato, iceberg lettuce and mayonnaise',
            discount_rate: 0.1,
            id: '1',
            name: 'Chicken & Avocado',
            photo:
              'https://chatfood-cdn.s3.eu-central-1.amazonaws.com/fe-code-challenge-1/chicken-avocado.jpg',
            price: 3500,
            stock: { availability: 3 },
            url: 'chicken-avocado',
          },
        ],
      },
    ];

    expect(currentResult).toEqual(expectedResult);
  });

  it('should handle empty result', () => {
    const formattedMenuData = formatMenuData({
      items: mockDataMenuItems,
      categories: mockDataCategories,
    });
    const currentResult = filterMenuData(formattedMenuData, 'error');

    expect(currentResult).toEqual([]);
  });
});
