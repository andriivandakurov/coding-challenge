import 'styled-components';

interface TypedObject<T> {
  [key: string]: T;
}

declare module 'styled-components' {
  export interface DefaultTheme {
    typography: TypedObject<string>;
    colors: TypedObject<string>;
    spacing: TypedObject<string>;
  }
}
