import { DefaultTheme } from 'styled-components';

const defaultTheme: DefaultTheme = {
  typography: {
    fontFamily: `'Inter', sans-serif`,
    fontSize: '14px',
  },
  colors: {
    selectedMenuItem: '#1258FF',
    cartSeparatorLine: '#F4F6F9',
    textPrimary: '#071C4D',
    textSecondary: '#838DA6',
    greyLight: '#EBEFF4',
  },
  spacing: {
    contentHorizontal: '21px',
  },
};

export { defaultTheme };
